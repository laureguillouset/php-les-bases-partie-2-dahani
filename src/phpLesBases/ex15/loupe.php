<?php

$text = file_get_contents($argv[1]);
$pattern = '/(?=").+?(?=")|(?<=">).+?(?=<)|(?<=com>).+?(?=<img)/';

$arrupper = [];

preg_match_all($pattern, trim($text), $matches);

// print_r($matches);

foreach ($matches[0] as $value) {
    array_push($arrupper, strtoupper($value));
}

// print_r($arrupper);

$newtext = str_replace($matches[0], $arrupper, $text);

echo $newtext;
