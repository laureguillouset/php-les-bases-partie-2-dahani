<?php

// $tab = array_splice($argv, 1);

if ($argc !== 4) {
    echo 'Incorrect Parameters' . "\n";
} else {
    $first = trim($argv[1]);
    $symbol = trim($argv[2]);
    $third = trim($argv[3]);

    if (is_numeric($first) && is_numeric($third)) {
        if ($symbol == '+') {
            $add = $first + $third;
            echo $add . "\n";
        } elseif ($symbol == '-') {
            $subs = $first - $third;
            echo $subs . "\n";
        } elseif ($symbol == '*') {
            $mul = $first * $third;
            echo $mul . "\n";
        } elseif ($symbol == '/') {
            if ($third != 0) {
                echo $first / $third . "\n";
            } else {
                echo '0' . "\n";
            }
        } elseif ($symbol == '%') {
            $modulo = $first % $third;
            echo $modulo . "\n";
        }
    } else {
        echo 'Incorrect Parameters' . "\n";
    }
}
