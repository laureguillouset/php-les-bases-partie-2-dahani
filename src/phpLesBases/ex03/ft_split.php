<?php

function ft_split($string)
{
    $arr = preg_split('/\W/', $string, -1, PREG_SPLIT_NO_EMPTY);
    sort($arr);

    return $arr;
}
