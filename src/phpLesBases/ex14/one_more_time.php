<?php

// Vérification que l'heure est correct
function check_24_timeFormat($time)
{
    if (preg_match('#((0([0-9])|(1[0-9]{1})|(2[0-3])):([0-5])([0-9]):([0-5])([0-9]))#', $time)) {
        return true;
    } else {
        return false;
    }
}

$jour = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'];

$chiffre_du_mois = ['janvier' => 1, 'février' => 2, 'mars' => 3, 'avril' => 4, 'mai' => 5, 'juin' => 6, 'juillet' => 7, 'août' => 8, 'septembre' => 9, 'octobre' => 10, 'novembre' => 11, 'décembre' => 12];

if (empty($argv[1])) {
    exit;
}

$tab = strtolower($argv[1]);
$arr = preg_split('/\W/', $tab, -1, PREG_SPLIT_NO_EMPTY);

if (count($arr) !== 7) {
    echo "Wrong Format\n";
    exit;
}

$time = $arr[4] . ':' . $arr[5] . ':' . $arr[6];

if (check_24_timeFormat($time) == false) {
    echo "Wrong Format\n";
    exit;
}

if (array_key_exists($arr[2], $chiffre_du_mois) == false) {
    echo "Wrong Format\n";
    exit;
}

$day = intval($arr[1]);
$month = intval(($chiffre_du_mois[$arr[2]]));
$year = intval($arr[3]);

if (checkdate($month, $day, $year) == false) {
    echo "Wrong Format\n";
    exit;
}

$time = $tab . ' heure normale d’Europe centrale';

$fmt = new IntlDateFormatter(
  'fr_FR',
  IntlDateFormatter::FULL,
  IntlDateFormatter::FULL,
);
$tparis = $fmt->parse($time);

if ($tparis == true) {
    echo $tparis . "\n";
} else {
    echo "Wrong Format\n";
}

// Vérification que l'heure est correct
// function check_24_timeFormat($tab)
// {
//   if (preg_match('#((0([0-9])|(1[0-9]{1})|(2[0-3])):([0-5])([0-9]):([0-5])([0-9]))#', $tab)) {
//     return true;
//   } else {
//     return false;
//   }
// }

// // $tz = reset(iterator_to_array(IntlTimeZone::createEnumeration('FR')));
// // $formatter = IntlDateFormatter::create(
// //   'fr_FR',
// //   IntlDateFormatter::FULL,
// //   IntlDateFormatter::FULL,
// //   $tz,
// //   IntlDateFormatter::GREGORIAN
// // );
// // $cal = IntlCalendar::createInstance($tz, '@calendar=islamic-civil');
// // date_default_timezone_set('CET');
// // $date = date('d-m-Y H:i:s', $arr2);

// if (!empty($argv[1])) {

//   $datetime = $argv[1] . 'heure normale d’Europe centrale';

//   $fmt = new IntlDateFormatter(
//     'fr_FR',
//     IntlDateFormatter::FULL,
//     IntlDateFormatter::FULL,
//     'France/Paris',
//     IntlDateFormatter::GREGORIAN

//   );
//   $tparis = $fmt->format($datetime);

//   echo "$tsparis\n";
// } else echo $argv[1];

//  // $date = date('Y-m-d H:i:s', $tsparis);

// //   $date = DateTime::createFromFormat('Y-m-d H:i:s', $date);
// //   if ($date) {
// //     echo $tsparis;
// //   } else {
// //     echo "Wrong Format";
// //   }
// // } else {
// //   echo "Wrong Format";
// // }

//   // if (checkdate($month,))
//   // function validateDate($date, $format = 'Y-m-d H:i:s')
//   // {
//   //   $d = DateTime::createFromFormat($format, $date);
//   //   return $d && $d->format($format) == $date;
//   // }

// //   $re1 = '((?:2|1)\\d{3}(?:-|\\/)(?:(?:0[1-9])|(?:1[0-2]))(?:-|\\/)(?:(?:0[1-9])|(?:[1-2][0-9])|(?:3[0-1]))(?:T|\\s)(?:(?:[0-1][0-9])|(?:2[0-3])):(?:[0-5][0-9]):(?:[0-5][0-9]))';    # Time Stamp 1

// //   if (preg_match_all("/" . $re1 . "/is", $date, $matches)) {
// //     //$timestamp1 = $matches[0][1];

// //     echo "$tsparis\n";
// //   } else echo "Wrong Format";
// // } else echo "Wrong Format";

// // //$tab = array_splice($argv, 1);
// // // //$date = $argv[1];
// // //$arr = preg_split('/\W/', $tab[0], -1,  PREG_SPLIT_NO_EMPTY);
// // //$tab2 = array_splice($arr, 1);
// // //$date = $tab2[0] . " " . $tab2[1] . " " . $tab2[2] . " " . $tab2[3] . ":" . $tab2[4] . ":" . $tab2[5];
// // function convert_date_fr($date, $format_in = 'j F Y H:i:s', $format_out = 'Y-m-d H:i:s')
// // {
// //   // French to english month names
// //   $months = array(
// //     'janvier' => 'january',
// //     'février' => 'february',
// //     'mars' => 'march',
// //     'avril' => 'april',
// //     'mai' => 'may',
// //     'juin' => 'june',
// //     'juillet' => 'july',
// //     'août' => 'august',
// //     'septembre' => 'september',
// //     'octobre' => 'october',
// //     'novembre' => 'november',
// //     'décembre' => 'december',
// //   );

// //   // List of available formats for date
// //   $formats_list = array('d', 'D', 'j', 'l', 'N', 'S', 'w', 'z', 'S', 'W', 'M', 'F', 'm', 'M', 'n', 't', 'A', 'L', 'o', 'Y', 'y', 'H', 'a', 'A', 'B', 'g', 'G', 'h', 'H', 'i', 's', 'u', 'v', 'F', 'e', 'I', 'O', 'P', 'T', 'Z', 'D', 'c', 'r', 'U');

// //   // We get separators between elements in $date, based on $format_in
// //   $split = str_split($format_in);
// //   $separators = array();
// //   $_continue = false;
// //   foreach ($split as $k => $s) {
// //     if ($_continue) {
// //       $_continue = false;
// //       continue;
// //     }
// //     // For escaped chars (like "\h")
// //     if ($s == '\\' && isset($split[$k + 1])) {
// //       $separators[] = '\\' . $split[$k + 1];
// //       $_continue = true;
// //       continue;
// //     }
// //     if (!in_array($s, $formats_list)) {
// //       $separators[] = $s;
// //     }
// //   }

// //   // Translate month name
// //   $tmp = preg_split('/(' . implode('|', array_map(function ($v) {
// //     if ($v == '/') {
// //       return '\/';
// //     }
// //     return str_replace('\\', '\\\\', $v);
// //   }, $separators)) . ')/', $date);

// //   foreach ($tmp as $k => $v) {
// //     $v = mb_strtolower($v, 'UTF-8');
// //     if (isset($months[$v])) {
// //       $tmp[$k] = $months[$v];
// //     }
// //   }

// //   // Re-construct the date
// //   $imploded = '';
// //   foreach ($tmp as $k => $v) {
// //     $imploded .= $v . (isset($separators[$k]) ? str_replace('\\', '', $separators[$k]) : '');
// //   }

// //   return DateTime::createFromFormat($format_in, $imploded)->format($format_out);
// // }
// // echo convert_date_fr('8-août 2018 14:30:54', 'j-F Y H:i:s', 'Y-m-d H:i:s');

// //   echo "$tsparis\n";
// // } else echo $argv[1];
// // $fmt = new IntlDateFormatter(
// //   'en_US',
// //   IntlDateFormatter::FULL,
// //   IntlDateFormatter::FULL,
// //   'America/Los_Angeles',
// //   IntlDateFormatter::GREGORIAN
// // );
// // echo 'First parsed output is ' . $fmt->parse('Wednesday, December 20, 1989 4:00:00 PM PT');

// //'Wednesday, December 20, 1989 4:00:00 PM PT';

// // $tsparis = $parser->parse('Wednesday, December 20, 1989 4:00:00 PM PT');

// // echo $tsparis;

// // ini_set('date.timezone', 'UTC');

// // /* La locale par défaut est prise depuis la configuration ini */
// // ini_set('intl.default_locale', 'fr_FR');

// // $cal = IntlCalendar::fromDateTime("2013-06-06 17:05:06 Europe/Dublin");
// // echo "défault :\n\t",
// // IntlDateFormatter::formatObject($cal),
// // "\n";

// // echo "long \$format (complet) :\n\t",
// // IntlDateFormatter::formatObject($cal, IntlDateFormatter::FULL),
// // "\n";

// // echo "array \$format (aucun, complet) :\n\t",
// // IntlDateFormatter::formatObject($cal, array(
// //   IntlDateFormatter::NONE,
// //   IntlDateFormatter::FULL
// // )),
// // "\n";

// // echo "string \$format (d 'de' MMMM y):\n\t",
// // IntlDateFormatter::formatObject($cal, "d 'de' MMMM y", 'en_US'),
// // "\n";

// // echo "avec DateTime :\n\t",
// // IntlDateFormatter::formatObject(
// //   new DateTime("2013-09-09 09:09:09 Europe/Madrid"),
// //   IntlDateFormatter::FULL,
// //   'es_ES'
// // ),
// // "\n";

// // $parser = new IntlDateFormatter(
// //   'fr_FR',
// //   IntlDateFormatter::FULL,
// //   IntlDateFormatter::FULL
// // );
// // $tsparis = $parser->parse($date);

// // var_dump($tsparis);

// // $datetime = DateTime::createFromFormat("d M Y H:i:s", $your_string_here);
// // $timestamp = $datetime->getTimestamp();

// // function dateToFrench($date, $format)
// // {

// //   $french_months = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');

// //   $english_months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

// //   return str_replace($english_months, $french_months, str_replace($english_days, $french_days, date($format, strtotime($date))));
// // }
// // // $str = new DateTime;

// // // $dt = new IntlDateFormatter('en_EN', IntlDateFormatter::SHORT, IntlDateFormatter::SHORT);
// // // $dt->setPattern('f d Y H:i:s');

// // // switch ($str) {
// // //   case 'fr':
// // //     setlocale(LC_TIME, 'fr_CA.UTF-8');
// // //     strftime("%d %B %G");
// // //   case 'en':
// // //     setlocale(LC_TIME, 'en_CA.UTF-8');
// // //     echo strftime("%B %e, %G");
// // //     break;
// // //   case 'fr':
// // //     setlocale(LC_TIME, 'fr_CA.UTF-8');
// // //     echo strftime("%e %B %G");
// // // }
// // //echo $dt->format($str);

// // //$format = "f d Y H:i:s";
// // //$format = "Y-f-l H:i:s";
// // // $dateobj = DateTime::createFromFormat($format, $dt->format($str));
// // // echo $date->format('Y-f-l H:i:s');
// // // echo $dateobj;
// // // $date = date_create($dateobj);
// // // echo date_timestamp_get($date);

// //  //<!-- $tab = array_splice($argv, 1);
// // //$str = preg_replace("/\s+/", ' ', $argv[1]);
// // // $format = "l-d-f-Y H:i:s";
// // // $dateobj = DateTime::createFromFormat($format, $str);
// // // echo $date->format('l-d-f-Y H:i:s'); -->

// // // // if ($tab = $dateobj) {
// // // // echo $date->format('Y-m-d');

// // // echo date_timestamp_get($dateobj);

// // // //echo (strtotime($dateobj))
// // // }

// // // setlocale(LC_TIME, 'fr_FR');
// // // date_default_timezone_set('Europe/Paris');
// // // echo utf8_encode(strftime('%A %d %B %Y, %H:%M'));

// // //var_dump($tab);
// // // $Jour_de_la_semaine = $argv[1];
// // // $Numéro_du_jour = $argv[2];
// // // $Mois = $argv[3];+
// // // $Année = $argv[4];
// // // $Heure = $argv[5];

// // // if ($argc == 6) {
// // // $date = preg_replace("/\s+/", ' ', $argv);
// // // var_dump($date);
// // // }

// // // if ($date = date_format($date, 'Y-m-d H:i:s')) {
// // // echo date_timestamp_get($date);
// // // }
// // // }

// // // // function get_ebay_UTC_8601(DateTime $time)
// // // // { $t = clone $time;
// // // // $t->setTimezone(new DateTimeZone("UTC"));
// // // // return $t->format("Y-m-d\TH:i:s\Z");
// // // // }
// // // $date = new DateTime("1899-12-31");
// // // // "-2209078800"
// // // echo $date->format("U");
