<?php

echo 'Entrez un nombre: ';
  while (true) {
      $nombre = rtrim(fgets(STDIN));

      if (is_numeric($nombre)) {
          if ($nombre % 2 == 0) {
              echo "Le chiffre $nombre est Pair\nEntrez un nombre: ";
          } else {
              echo "Le chiffre $nombre est Impair\nEntrez un nombre: ";
          }
      } else {
          echo "'$nombre' n'est pas un chiffre\nEntrez un nombre: ";
      }
  }
