# simplon-dev-i-web-php-les-bases
[![pipeline status](https://gitlab.com/sim39/simplon-dev-i-web-php-les-bases/badges/main/pipeline.svg)](https://gitlab.com/sim39/simplon-dev-i-web-php-les-bases/-/commits/main) [![coverage report](https://gitlab.com/sim39/simplon-dev-i-web-php-les-bases/badges/main/coverage.svg)](https://gitlab.com/sim39/simplon-dev-i-web-php-les-bases/-/commits/main)



## Getting started

```bash
docker compose build
docker compose up -d
```

### SSH docker instance connection
```bash
docker compose exec php zsh 
```

### Run test locally

```bash
docker compose exec php composer test-locally
```

### Run lint

```bash
docker compose exec php composer lint
```
